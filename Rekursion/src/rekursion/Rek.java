/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rekursion;

/**
 *
 * @author Hannes
 */
public class Rek {
private int f;

    public int getF() {
        return f;
    }

    public void setF(int f) {
        this.f = f;
    }
    


    public int rek(int i){
        if(i==1){
            return 1;
        }
        else{
            return rek(i-1)*i;
        }   
     
    }
    
    }

