package autohaus;

import java.io.Console;

public class LinkedList {

    private Auto first;
    private int size;

    public LinkedList(Auto auto) {
        first = auto;
        size = 1;

    }

    public void addCar(Auto node) {

        Auto tmp = first;
        while (tmp.getNext() != null) {
            tmp = tmp.getNext();
        }
        tmp.setNext(node);
        this.size++;
    }

    public void addCarSortet(Auto node) {

        addCar(node);
        sort();

    }

    public Auto getCar(int position) {
        Auto tmp = first;
        for (int i = 0; i < position; i++) {
            tmp = tmp.getNext();
        }

        return tmp;
    }

    public void printList() {
        Auto tmp = first;
        System.out.println(tmp.toString());
        while (tmp.getNext() != null) {
            tmp = tmp.getNext();
            System.out.println(tmp.toString());

        }

    }

    public void addCarOnIndex(int position, Auto node) {
        Auto tmp = first;
        int count = 0;
        while (tmp.getNext() != null && count < position - 1 && count < size) {
            tmp = tmp.getNext();
            count++;
        }
        Auto temp = first;
        if (count < size) {
            node.setNext(tmp);

        }
        count = 0;
        while (temp.getNext() != null && count < position - 2 && count < size) {
            temp = temp.getNext();
            count++;
        }
        temp.setNext(node);
        this.size++;
    }

    public void addfirst(Auto node) {
        node.setNext(first);
        first = node;
        size++;
    }

    public Auto getCarOnIndex(int a) {
        Auto temp = first;
        int count = 0;
        while (temp != null) {
            if (count == a) {
                return temp;
            }
            temp = temp.getNext();
            count++;
        }
        return temp;
    }

    public void sort() {
        Auto tmp = first;
        Auto vorher = null;
        for (int t = 0; t < this.size; t++) {
            for (int i = 0; i < this.size - t - 1; i++) {

                if (tmp.getKosten() > tmp.getNext().getKosten()) {
                    if (vorher != null) {
                        vorher.setNext(tmp.getNext());
                        tmp.setNext(vorher.getNext().getNext());
                        vorher.getNext().setNext(tmp);
                        tmp = vorher.getNext();
                    } else {
                        Auto a = tmp.getNext();
                        tmp.setNext(tmp.getNext().getNext());
                        a.setNext(tmp);

                        first = a;
                    }

                }

                vorher = tmp;
                tmp = tmp.getNext();

            }
            vorher = null;
            tmp = first;
        }
    }

    public void deleteCar(int position) {
        Auto tmp = first;
        position--;
        for (int i = 0; i < position - 1; i++) {
            tmp = tmp.getNext();
        }
        if (this.size >= 1 && position != 0) {
            tmp.setNext(tmp.getNext().getNext());
        } else if (position == 0) {
            first = tmp.getNext();
        } else {
            first = null;
        }
        this.size--;

    }

    public int getSize() {
        return this.size;
    }

}
