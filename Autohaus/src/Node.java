/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hannes
 */
public class Node {
    
    private Auto Auto;
    private Node next = null;

    public Node(Auto value) {
        this.Auto = value;
    }


  

 
    
    public Auto getValue() {
        return Auto;
    }

    public void setValue(Auto value) {
        this.Auto = value;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
    
    
}
