/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg4gewinnt;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;
import java.util.Vector;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.*;
import javax.swing.border.Border;

/**
 *
 * @author Hannes
 */
public class GewinntGui extends JFrame implements MouseListener{

    private JPanel mainPanel = null;

    private JLabel[][] Gfelder;
    private JButton[] Gbutton;
    private int feld0 = 5;
    private int feld1 = 5;
    private int feld2 = 5;
    private int feld3 = 5;
    private int feld4 = 5;
    private int feld5 = 5;
    private int feld6 = 5;
    private int counter = 0;
    public GewinntGui() {
        initialiseGui();
    

    }

    public void initialiseGui() {
        mainPanel = new JPanel();
        mainPanel.setLayout(null);
        this.setContentPane(mainPanel);

        this.setSize(706, 730);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        Border border = BorderFactory.createLineBorder(Color.WHITE,1);
        
        
        Gbutton = new JButton[7];
        int bbreite = 0;
        
        for (int i = 0; i < 7; i++) {
            Gbutton[i]=new JButton();
          Gbutton[i].setBounds(bbreite,0,100,100);
          bbreite+=100;
          mainPanel.add(Gbutton[i]);
          Gbutton[i].addMouseListener(this);
        }
        
        
        int hoehe = 0;
int breite = 0;
        Gfelder = new JLabel[6][7];
        for (int i = 0; i < 6; i++) {
            hoehe+=100;
            breite=0;
            for (int j = 0; j < 7; j++) {
                Gfelder[i][j] = new JLabel();
                Gfelder[i][j].setOpaque(true);
                Gfelder[i][j].setBounds(breite, hoehe, 100, 100);
                Gfelder[i][j].setBorder(border);
                Gfelder[i][j].setBackground(Color.darkGray);
                breite+=100;
                mainPanel.add(Gfelder[i][j]);
            }
            
        }

   
        
    }
private void überprüfung(){
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 7; j++) {
                if(j+3<6&&Gfelder[i][j].getBackground()==(Color.MAGENTA)&&Gfelder[i][j+1].getBackground()==(Color.MAGENTA)&&Gfelder[i][j+2].getBackground()==(Color.MAGENTA)&&Gfelder[i][j+3].getBackground()==(Color.MAGENTA)){
    for (int k = 0; k < 6; k++) {
                        for (int l = 0; l < 7; l++) {
                            Gfelder[k][l].setBackground(Color.darkGray);
                            feld1 = 5;
                            feld2 = 5;
                            feld3 = 5;
                            feld4 = 5;
                            feld5 = 5;
                            feld6 = 5;
                            feld0 = 5;
                        }
                    }                }
                if(j+3<6&&Gfelder[i][j].getBackground()==(Color.black)&&Gfelder[i][j+1].getBackground()==(Color.black)&&Gfelder[i][j+2].getBackground()==(Color.black)&&Gfelder[i][j+3].getBackground()==(Color.black)){
for (int k = 0; k < 6; k++) {
                        for (int l = 0; l < 7; l++) {
                            Gfelder[k][l].setBackground(Color.darkGray);
                            feld1 = 5;
                            feld2 = 5;
                            feld3 = 5;
                            feld4 = 5;
                            feld5 = 5;
                            feld6 = 5;
                            feld0 = 5;
                        }
                    }                }
                if(i+3<6&&Gfelder[i][j].getBackground()==(Color.MAGENTA)&&Gfelder[i+1][j].getBackground()==(Color.MAGENTA)&&Gfelder[i+2][j].getBackground()==(Color.MAGENTA)&&Gfelder[i+3][j].getBackground()==(Color.MAGENTA)){
                    for (int k = 0; k < 6; k++) {
                        for (int l = 0; l < 6; l++) {
                            Gfelder[k][l].setBackground(Color.darkGray);
                            feld1 = 5;
                            feld2 = 5;
                            feld3 = 5;
                            feld4 = 5;
                            feld5 = 5;
                            feld6 = 5;
                            feld0 = 5;
                        }
                    }
                }
                     if(i+3<6&&Gfelder[i][j].getBackground()==(Color.black)&&Gfelder[i+1][j-1].getBackground()==(Color.black)&&Gfelder[i+2][j-2].getBackground()==(Color.black)&&Gfelder[i+3][j-3].getBackground()==(Color.black)){
                    for (int k = 0; k < 6; k++) {
                        for (int l = 0; l < 7; l++) {
                            Gfelder[k][l].setBackground(Color.darkGray);
                            feld1 = 5;
                            feld2 = 5;
                            feld3 = 5;
                            feld4 = 5;
                            feld5 = 5;
                            feld6 = 5;
                            feld0 = 5;
                        }
                    }
                }
         if(i+3<6&&j+3<7&&Gfelder[i][j].getBackground()==(Color.black)&&Gfelder[i+1][j-1].getBackground()==(Color.black)&&Gfelder[i+2][j].getBackground()==(Color.black)&&Gfelder[i+3][j].getBackground()==(Color.black)){
                    for (int k = 0; k < 6; k++) {
                        for (int l = 0; l < 7; l++) {
                            Gfelder[k][l].setBackground(Color.darkGray);
                            feld1 = 5;
                            feld2 = 5;
                            feld3 = 5;
                            feld4 = 5;
                            feld5 = 5;
                            feld6 = 5;
                            feld0 = 5;
                        }
                    }
                }
    }
    }
}



    private void grünPunktsetzen(int i){
        
        if(i == 0){
        if(feld0>=0){
             Gfelder[feld0][i].setBackground(Color.black);
             feld0--;
        }
       }
         if(i == 1){
        if(feld1>=0){
             Gfelder[feld1][i].setBackground(Color.black);
             feld1--;
        }
       }
 if(i == 2){
        if(feld2>=0){
             Gfelder[feld2][i].setBackground(Color.black);
             feld2--;
        }
       }
 if(i == 3){
        if(feld3>=0){
             Gfelder[feld3][i].setBackground(Color.black);
             feld3--;
        }
       }
 if(i == 4){
       if(feld4>=0){
             Gfelder[feld4][i].setBackground(Color.black);
             feld4--;
        }
       }
 if(i == 5){
        if(feld5>=0){
             Gfelder[feld5][i].setBackground(Color.black);
             feld5--;
        }
 }
  if(i == 6){
         if(feld6>=0){
             Gfelder[feld6][i].setBackground(Color.black);
             feld6--;
        }
       }


    
    
    
überprüfung();
    }
    private void rotPunktsetzen(int i){

        if(i == 0){
        if(feld0>=0){
             Gfelder[feld0][i].setBackground(Color.MAGENTA);
             feld0--;
        }
       }
         if(i == 1){
        if(feld1>=0){
             Gfelder[feld1][i].setBackground(Color.MAGENTA);
             feld1--;
        }
       }
 if(i == 2){
        if(feld2>=0){
             Gfelder[feld2][i].setBackground(Color.MAGENTA);
             feld2--;
        }
       }
 if(i == 3){
        if(feld3>=0){
             Gfelder[feld3][i].setBackground(Color.MAGENTA);
             feld3--;
        }
       }
 if(i == 4){
       if(feld4>=0){
             Gfelder[feld4][i].setBackground(Color.MAGENTA);
             feld4--;
        }
       }
 if(i == 5){
        if(feld5>=0){
             Gfelder[feld5][i].setBackground(Color.MAGENTA);
             feld5--;
        }
 }
  if(i == 6){
         if(feld6>=0){
             Gfelder[feld6][i].setBackground(Color.MAGENTA);
             feld6--;
        }
       }


    
    
    überprüfung();
}

    @Override
    public void mouseClicked(MouseEvent e) {
        for (int i = 0; i < 7; i++) {
            if(Gbutton[i].equals(e.getSource()))
                if(counter % 2 == 0){
                rotPunktsetzen(i);    
                counter++;
                }else{
                    grünPunktsetzen(i);
                counter++;
                }
        }
        überprüfung();
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

}
