package gui;


import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hannes
 */
public class TaschenrechnerGUI extends JFrame implements MouseListener {
    
  
    private JPanel mainPanel = null;
    private JLabel lbl_zahl1;
    private JLabel lbl_zahl2;
    private JTextField txt_zahl1;
    private JTextField txt_zahl2;
    private JButton btn_plus;
    private JButton btn_minus;
    private JButton btn_mal;
    private JLabel lbl_erg;
      
    public TaschenrechnerGUI(){
        initializeGUI();
    
    }
    private void initializeGUI(){
        
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                this.setSize(500, 500);
                  mainPanel = new JPanel();
                mainPanel.setLayout(null);
               
                 lbl_zahl1 = new JLabel();
                lbl_zahl1.setText("zahl 1");
                lbl_zahl1.setBounds(125, 35, 50, 10);
                
                
                 lbl_zahl2 = new JLabel();
                lbl_zahl2.setText("zahl 2");
                lbl_zahl2.setBounds(125, 65, 50, 10);
                
                 txt_zahl1 = new JTextField();
                txt_zahl1.setBounds(185, 30, 50, 15);
               
                 txt_zahl2 = new JTextField();
                txt_zahl2.setBounds(185, 65, 50, 15);
                
                 btn_plus = new JButton();
                 btn_plus.setBounds(100, 90, 90, 30);
                 btn_plus.setText("Plus");
                 
                 btn_minus = new JButton();
                btn_minus.setText("MINUS");
                   btn_minus.setBounds(200, 90, 90, 30);
                   
                 btn_mal = new JButton();
                btn_mal.setText("Mal");
               btn_mal.setBounds(300, 90, 90, 30);
                
               btn_mal.addMouseListener(this);
               btn_plus.addMouseListener(this);
               btn_minus.addMouseListener(this);
                       
                       
                 lbl_erg = new JLabel();
                lbl_erg.setText("Ergebniss");
                lbl_erg.setBounds(180, 155, 80, 35);
                
                
                mainPanel.add(lbl_zahl1);
                mainPanel.add(lbl_zahl2);
                mainPanel.add(txt_zahl1);
                mainPanel.add(txt_zahl2);
                mainPanel.add(btn_plus);
                mainPanel.add(btn_minus);
                mainPanel.add(btn_mal);
                mainPanel.add(lbl_erg);
                
                        
                        
                        setContentPane(mainPanel);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        try{
        int Zahl1 = Integer.parseInt(txt_zahl1.getText());
        int Zahl2 = Integer.parseInt(txt_zahl2.getText());
        
        if(e.getSource().equals(btn_plus)){
            int summe = Zahl1 + Zahl2;
            lbl_erg.setText("Ergebnis: " +summe);
        }else if(e.getSource().equals(btn_minus)){
             int minus = Zahl1 - Zahl2;
            lbl_erg.setText("Ergebnis: " +minus);
        }else if(e.getSource().equals(btn_mal)){
            int mal = Zahl1 * Zahl2;
         lbl_erg.setText("Ergebnis: " +mal);
        }
        
        
        }catch (Exception ex){
            JOptionPane.showMessageDialog(null, "Gib sto Zahlen ein");
        }
        
        }

    @Override
    public void mousePressed(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent e) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent e) {
     //   throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent e) {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
