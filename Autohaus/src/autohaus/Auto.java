package autohaus;

public class Auto {

    private String hersteller;
    private long Laufleistung ;
    private double kosten; 
    private String farbe;
    private boolean Unfallwagen;
    private String krafstoff;
    private double leistung; 
   
    private Auto next = null;

    public Auto(String hersteller, long laufleistung, double kosten, String farbe, boolean unfallwagen, String krafstoff, double leistung) {
        this.hersteller = hersteller;
        this.Laufleistung = laufleistung;
        this.kosten = kosten;
        this.farbe = farbe;
        this.Unfallwagen = unfallwagen;
        this.krafstoff = krafstoff;
        this.leistung = leistung;

    }

    @Override
    public String toString() {
        String ausgabe = "";
        ausgabe+="---\n";
        ausgabe+="Hersteller: " + this.hersteller+"\n";
        ausgabe+="Preis: "+ this.kosten +" €\n";
        ausgabe+="Motor: "+this.leistung+" PS ("+this.krafstoff+")\n"; 
        ausgabe+="KM-Stand: "+this.Laufleistung+"\n";
        ausgabe+="Farbe: "+this.farbe+"\n";
        if(Unfallwagen==true)
            ausgabe+="unfallwagen \n";
        else
            ausgabe+="unfallfrei\n";
        ausgabe+="---\n";
        return ausgabe;
       
        
    }

    public String getHersteller() {
        return hersteller;
    }

    public void setHersteller(String hersteller) {
        this.hersteller = hersteller;
    }

    public long getLaufleistung() {
        return Laufleistung;
    }

    public void setLaufleistung(long Laufleistung) {
        this.Laufleistung = Laufleistung;
    }

    public double getKosten() {
        return kosten;
    }

    public void setKosten(double kosten) {
        this.kosten = kosten;
    }

    public String getFarbe() {
        return farbe;
    }

    public void setFarbe(String farbe) {
        this.farbe = farbe;
    }

    public boolean isUnfallwagen() {
        return Unfallwagen;
    }

    public void setUnfallwagen(boolean Unfallwagen) {
        this.Unfallwagen = Unfallwagen;
    }

    public String getKrafstoff() {
        return krafstoff;
    }

    public void setKrafstoff(String krafstoff) {
        this.krafstoff = krafstoff;
    }

    public double getLeistung() {
        return leistung;
    }

    public void setLeistung(double leistung) {
        this.leistung = leistung;
    }

   

    public Auto getNext() {
        return next;
    }

    public void setNext(Auto next) {
        this.next = next;
    }

}
