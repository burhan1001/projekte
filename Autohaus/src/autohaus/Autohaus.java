package autohaus;

public class Autohaus {

    public static void main(String[] args) {
        //create List
        LinkedList autoListe = new LinkedList(new Auto("Ford", 125000, 7999.99, "silber metallic", false, "Diesel", 101.0));

        //Create Autos
        Auto b = new Auto("BMW", 80000, 34000, "matt sxhwarz", true, "Benzin", 273.0);
        Auto c = new Auto("Fiat", 200000, 1500.99, "weiß", true, "Diesel", 120);
        Auto d = new Auto("Porsche", 54000, 120000, "grey", false, "Benzin", 340);

        //Add Autos to List
        autoListe.addCar(b);
        autoListe.addCar(c);
        autoListe.addCar(d);

        //sort List
        autoListe.sort();
        //print list
        autoListe.printList();

    }

}
