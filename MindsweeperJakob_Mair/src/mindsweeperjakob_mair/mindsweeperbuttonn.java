/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mindsweeperjakob_mair;

import java.awt.Color;
import javax.swing.JButton;

/**
 *
 * @author Jakob Mair
 */
public class mindsweeperbuttonn extends JButton {

    private int zahl;
    private boolean isbomb = false;
    private int posx = 0;
    private int posy = 0;

    public mindsweeperbuttonn(int zahl, int x, int y) {
        super();
        this.posx = x;
        this.posy = y;
        this.zahl = zahl;;
    }

    public void erhoeZahl() {
        zahl++;

    }

    public void show() {

        if (isbomb) {
            this.setText("B");
            this.setForeground(Color.red);
        } else {

            if (zahl == 0) {
                this.setText(" ");
            } else {
                this.setText(zahl + " ");
            }
        }
    }

    public int getposX() {
        return posx;
    }

    public int getposY() {
        return posy;
    }

    public int getZahl() {
        return zahl;
    }

    public void setZahl(int zahl) {
        this.zahl = zahl;
    }

    public boolean isbomb() {
        return isbomb;
    }

    public void setIsbomb(boolean isbomb) {
        this.isbomb = isbomb;
    }

}
