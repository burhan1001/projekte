/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstaktflugzeug;

/**
 *
 * @author Hannes Burak
 */
 class Doppeldecker extends Flugzeug{

    public final int Flügelpaare = 2;
    public int Loopingspeed = 320; 
    public  boolean Cockpit;
     public Doppeldecker(String hersteller, int maxSpeed, int anzahlFluegel, boolean Cockpitoffen) {
        super(hersteller, maxSpeed, anzahlFluegel);
        this.Cockpit = Cockpitoffen;
    }

    public Doppeldecker(boolean Cockpit, String hersteller, int maxSpeed, int anzahlFluegel) {
     super(hersteller, maxSpeed, anzahlFluegel);
     this.Cockpit = true;
    }
    
   
     public boolean isOffensesCockpit(){
     return Cockpit;
     
     
     }
     
     
     
    @Override
    public boolean getLooping() {
      if(super.getMaxSpeed()>this.Loopingspeed){
          return true;
      
    }else{
          return false;
      }
    
}
 
 
 
 
 
 
 }
