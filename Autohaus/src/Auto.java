/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Hannes
 */
public class Auto {

   
   
      private String Hersteller;
    private long km;
    private double EUR;

    public String getHersteller() {
        return Hersteller;
    }

    public void setHersteller(String Hersteller) {
        this.Hersteller = Hersteller;
    }

    public long getKm() {
        return km;
    }

    public void setKm(long km) {
        this.km = km;
    }

    public double getEUR() {
        return EUR;
    }

    public void setEUR(double EUR) {
        this.EUR = EUR;
    }

    public String getFarbe() {
        return Farbe;
    }

    public void setFarbe(String Farbe) {
        this.Farbe = Farbe;
    }

    public boolean isUnfallwagen() {
        return Unfallwagen;
    }

    public void setUnfallwagen(boolean Unfallwagen) {
        this.Unfallwagen = Unfallwagen;
    }

    public String getKraftstoff() {
        return Kraftstoff;
    }

    public void setKraftstoff(String Kraftstoff) {
        this.Kraftstoff = Kraftstoff;
    }

    public double getPs() {
        return Ps;
    }

    public void setPs(double Ps) {
        this.Ps = Ps;
    }
    private String Farbe;
    private boolean Unfallwagen;
    private String Kraftstoff;
    private double Ps;

    public Auto(String Hersteller, long km, double EUR, String Farbe, boolean Unfallwagen, String Kraftstoff, double Ps) {
        this.Hersteller = Hersteller;
        this.km = km;
        this.EUR = EUR;
        this.Farbe = Farbe;
        this.Unfallwagen = Unfallwagen;
        this.Kraftstoff = Kraftstoff;
        this.Ps = Ps;
    }
    
    
     @Override
    public String toString() {
        return " ---\n" + " Hersteller: " + Hersteller + ",\n km: " + km + ",\n EUR: " + EUR + ",\n Farbe: " + Farbe + ",\n Unfallwagen: " + Unfallwagen + ",\n Kraftstoff: " + Kraftstoff + ",\n Ps=" + Ps + "\n ---";
    }
    
  


 
 }

   




