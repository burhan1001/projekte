/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstaktflugzeug;

/**
 *
 * @author Hannes Burak
 */
 class Verkehrsflugzeug extends Flugzeug{

     private final int Flügelpaar = 1;
     private int AnzahlPassagiere = 0;
     
     
     public Verkehrsflugzeug(String hersteller, int maxSpeed, int anzahlFluegel, String Immarikulationsnummer, int AnzahlPassagiere) {
        super(hersteller, maxSpeed, anzahlFluegel);
        this.AnzahlPassagiere = AnzahlPassagiere;
        super.setImmatNummer(Immarikulationsnummer);
    }

    public int getAnzahlPassagiere() {
        return AnzahlPassagiere;
    }

    public void setAnzahlPassagiere(int AnzahlPassagiere) {
        this.AnzahlPassagiere = AnzahlPassagiere;
    }

     
     
    @Override
    public boolean getLooping() {
        return false;
    }

   

    
    
}
