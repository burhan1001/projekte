/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mindsweeperjakob_mair;

/**
 *
 * @author Jakob
 */
public class msbutton {

    private int zahl;
    private boolean isbomb = false;
    private int x = 0;
    private int y = 0;

    public msbutton(int zahl, int x, int y) {

        super();
        this.x = x;
        this.y = y;
        this.zahl = zahl;

    }

    public void setIsbomb(boolean isbomb) {
        this.isbomb = isbomb;
    }

    public int getZahl() {
        return zahl;
    }

    public boolean isIsbomb() {
        return isbomb;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
